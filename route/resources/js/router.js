import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    // {
    //     path: '/about',
    //     name: 'About',
    //     // route level code-splitting
    //     // this generates a separate chunk (about.[hash].js) for this route
    //     // which is lazy-loaded when the route is visited.
    //     component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    // },
    {
        path: '/vue/test/4',
        name: 'VueSectionA',
        component: () => import("./components/SectionA")
    },
    {
        path: '/vue/test/5',
        name: 'VueSectionB',
        component: () => import("./components/SectionB")
    },
    {
        path: '/vue/test/6',
        name: 'VueSectionC',
        component: () => import("./components/SectionC"),
        children: [
            {
                path: 'area/D',
                name: 'VueAreaD',
                component: () => import("./components/AreaD")
            },
            {
                path: 'area/E',
                name: 'VueAreaE',
                component: () => import("./components/AreaE")
            }
        ]
    },
    {
        path: '/lv/test/10',
        name: 'LVSectionA',
        component: () => import("./components/SectionA")
    },
    {
        path: '/lv/test/11',
        name: 'LVSectionB',
        component: () => import("./components/SectionB")
    },
    {
        path: '/lv/test/12',
        name: 'LVSectionC',
        component: () => import("./components/SectionC"),
        children: [
            {
                path: 'area/D',
                name: 'LVAreaD',
                component: () => import("./components/AreaD")
            },
            {
                path: 'area/E',
                name: 'LVAreaE',
                component: () => import("./components/AreaE")
            }
        ]
    },
    {
        path: '/mix1/test/7',
        component: () => import("./components/SectionA")
    },
    {
        path: '/mix2/test/8',
        component: () => import("./components/SectionB")
    },
    {
        path: '/mix3/test/9',
        component: () => import("./components/SectionC")
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
