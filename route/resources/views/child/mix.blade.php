@extends('layouts.welcome')

@section('content-side')
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link" href="/mix1/test/7">Test 7</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/mix2/test/8">Test 8</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/mix3/test/9">Test 9</a>
        </li>
    </ul>
@stop

@section('content')
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">This is Laravel + Vue</h5>
            <a href="#" class="card-link">Card link</a>
            <a href="#" class="card-link">Another link</a>
        </div>
    </div>
@stop