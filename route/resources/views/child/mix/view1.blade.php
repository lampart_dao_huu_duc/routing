@extends('layouts.welcome')

@section('content-side')
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link" href="/mix1/test/7">Test 7</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/mix2/test/8">Test 8</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/mix3/test/9">Test 9</a>
        </li>
    </ul>
@stop

@section('content')
    <div class="row">
        <div class="col-6">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">This is View 1</h5>
                    <p class="card-text">Mix routing</p>
                    <btn class="btn btn-primary">Hello</btn>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div id="app">
                <router-view></router-view>
            </div>
        </div>
    </div>
@stop