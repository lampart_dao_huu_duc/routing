@extends('layouts.welcome')

@section('content-side')
    @if (isset($isVue) )
    @elseif (isset($isLV))
    @else
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="/laravel/test/1">Test 1</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/laravel/test/2">Test 2</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/laravel/test/3">Test 3</a>
            </li>
        </ul>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">This is Laravel</h5>
                    <p class="card-text">Laravel routing</p>
                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">This is test {{ $section }}</h5>
                    <p class="card-text">Laravel routing</p>
                    <a id="value" class="This is section {{ $section }}" href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                </div>
            </div>
        </div>
    </div>
    <script>
        //alert(document.getElementById('value').className)
    </script>
@stop