<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('layouts.welcome');
});

Route::get('/laravel', function () {
    return view('child.content');
});

Route::get('/vue', function () {
    return view('child.content')->with('isVue', true);
});

Route::get('/lv', function () {
    return view('child.content')->with('isLV', true);
});

Route::prefix('/laravel')->group(function () {
    Route::get('/test/{section}', function ($section) {
        return view('child.section')->with('section', $section);
    });
});

Route::get('/mix', function () {
    return view('child.mix');
});

Route::get('/mix1/{vue_capture?}', function () {
    return view('child.mix.view1');
})->where('vue_capture', '.*');

Route::get('/mix2/{vue_capture?}', function () {
    return view('child.mix.view2');
})->where('vue_capture', '.*');

Route::get('/mix3/{vue_capture?}', function () {
    return view('child.mix.view3');
})->where('vue_capture', '.*');
